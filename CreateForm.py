import rdflib
import sys
from rdflib import BNode, URIRef
from rdflib.namespace import RDFS
import modules as wsCreate

def createForm(specs):

    fout = open(specs["formOutFile"], 'w')
    sys.stdout = fout

    baseUrl=specs["baseUrl"]
    myns = specs['ns']
    wsCreate.printHeader(baseUrl, "fm")

    ## Usage
    for form in list(specs['form']):
        modelFile = form['desc']['modelFile']
        ontClass =form['desc']['ontClass']
        annoFile = form['desc']['annoFile']
        createObjectForm(myns, ontClass,modelFile, annoFile )

    fout.close()


def createFieldCardinalityDict(ontClass, modelFile):
    g = rdflib.Graph()
    g.parse(modelFile, format="n3")

    qres = g.query(
        """SELECT DISTINCT ?p ?b
        WHERE { %s ?p ?b .
         }""" % ontClass)

    onProperty = ""
    cardinality = ""
    restrictions = {}
    for (p, b) in qres:
        if isinstance(b, BNode):
            for predicate, object in g.predicate_objects(b):
                if ("onProperty" in str(predicate)):
                    onProperty = str(object)
                elif ("Cardinality" in str(predicate).strip()):
                    cardinality = int(object)
        restrictions[onProperty] = cardinality
    g.close()
    return restrictions


### get ranges
def getRangeDict(restrictionDict, modelFile):
    g = rdflib.Graph()
    g.parse(modelFile, format="n3")

    fieldTypes = {}
    for k in restrictionDict:
        fieldTypes[k] = []
        fieldTypes[k] = g.value(URIRef(k), RDFS.range)
    g.close()
    return fieldTypes


## mmm annotation
def createFieldAnnotationDict(fieldDict, annotationFile):
    g2 = rdflib.Graph()
    g2.parse(annotationFile, format="n3")
    annotations = {}
    for k in fieldDict:
        annotations[k] = []
        for predicate, object in g2.predicate_objects(URIRef(k)):
            annotations[k] = str(object)
    g2.close()
    return annotations


## create field specs
def createControlSpec(ns, k, i, dimension, cardinality, range):
    if "LiteralDimension" in dimension:
        print("rdf:type form:TextFieldSpec ;")
        print("form:forDimension <%s> ;" % k)
    elif "Relationship" in dimension:
        print("rdf:type form:ObjectFieldSpec ;")
        print("form:forRelationship <%s> ;" % k)

    if range is not None:
        if "<"  not in str(range):
            print("form:allowedDataType <%s> ;" % range)
        else:
            print("form:allowedDataType %s ;" % range)
    else:
        print("form:allowedDataType xsd:string ;")
    try:
        if int(cardinality) >= 1:
            print("form:mandatory 'true'^^xsd:boolean ;")
        else:
            print("form:mandatory 'false'^^xsd:boolean ;")
    except ValueError:
        print("form:mandatory 'false'^^xsd:boolean ;")

    print("form:editable 'true'^^xsd:boolean ;\r\n" +
          "form:newRow 'true'^^xsd:boolean ;\r\n" +
          "form:order %s ;\r\n" % i +
          "rdfs:label 'Label' .\r\n")


## create object form
def createObjectForm(myns, ontClassIri, modelFile, annotationFile):
    cardinalityDict = createFieldCardinalityDict(ontClassIri, modelFile)
    dimensionDict = createFieldAnnotationDict(cardinalityDict, annotationFile)
    rangeDict = getRangeDict(cardinalityDict, modelFile)

    entityName = ontClassIri.split("#")[-1].replace(">", "")
    print("%s:%sFormSpec\r\n" % (myns, entityName) +
          "rdf:type form:ObjectFormSpec ; \r\n" +
          "form:rootPanelSpec %s:%sPanelSpec ; \r\n" % (myns, entityName) +
          "rdfs:label 'Create %s form spec'  .\r\n" % entityName)

    print("%s:%sPanelSpec\r\n" % (myns, entityName) +
          "rdf:type form:SimplePanelSpec ;\r\n" +
          "form:rootClass %s;" % (ontClassIri))

    for k in dimensionDict:
        fieldName = k.split("#")[-1]
        print("form:controlSpec %s:%sFieldSpec ;" % (myns, fieldName))
    print("rdfs:label 'Create %s panel spec' .\r\n" % entityName)

    for i, k in enumerate(dimensionDict):
        fieldName = k.split("#")[-1]
        print("%s:%sFieldSpec " % (myns, fieldName))
        createControlSpec(myns, k, i + 1, dimensionDict[k], cardinalityDict[k], rangeDict[k])
