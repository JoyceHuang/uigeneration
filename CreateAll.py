from CreateTable import createTable
from CreateWs import  createWs
from CreateForm import  createForm
from CreateVerb import  createVerb
import json

from jsoncomment import JsonComment

parser = JsonComment(json)

with open('workspace-spec') as infile:
    specs = parser.load(infile)

createVerb(specs)
createTable(specs)
createWs(specs)
createForm(specs)

infile.close()
