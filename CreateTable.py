import sys
import json
import modules as wsCreate
from jsoncomment import JsonComment

parser = JsonComment(json)

def createTable(specs):
    fout = open(specs["tableOutFile"], 'w')
    sys.stdout = fout

    baseUrl=specs["baseUrl"]
    myns = specs['ns']

    tableFile = specs['tableSpecFile']
    with open(tableFile) as tableSpecInfile:
        tbSpecs = parser.load(tableSpecInfile, strict=False)

    wsCreate.printHeader(baseUrl, "tb")
    variableList=[]

    tables = tbSpecs["tables"]
    for table in tables:
        print("\r\n%s:%sTableDesign"%(myns,table["name"]))
        print(" rdf:type set:TableDesign ;")
        print(" set:tableTitle '%s'^^xsd:string ;"%table["name"])
        print(" set:associatedQuery %s:%sSelectQuery ;"%(myns, table["name"]) )
        print(" set:associatedWithDatasetTemplate %s:%s ;" % (myns, specs["datasetTemplate"]))
        print("rdfs:label '%s design'^^xsd:string ;"%(table["name"]))

        variableList.clear()
        for cols in table["column"]:
            print(" set:column %s:%sColumn ;"%(myns, cols["name"]))
        print(".")

        for index, cols in enumerate(table["column"]):
            print("%s:%sVariable" % (myns, cols["name"]))
            print(" rdf:type set:Variable ;")
            print(" set:variable '?%s'^^xsd:string ;" % (cols["name"]))
            variableList.append(cols["name"])
            print(" set:variableType %s ;" % (cols["value"]))
            print("  rdfs:label '?%s variable' ." % (cols["name"]))
            print("\r\n")

            print("%s:%sColumn" % (myns, cols["name"]))
            print("  rdf:type set:TableColumn ;")
            print(" set:boundToVariable %s:%sVariable ;" %(myns, cols["name"]))
            print(" set:order %s ;" % str(index+1))
            print(" rdfs:label '%s'^^xsd:string .\r\n" %(cols["name"]))

        print("%s:%sSelectQuery " % (myns, table["name"]))
        print("rdf:type set:SelectQuery ;\r\n" +
                  " set:SPARQLNotation \"\"\"%s\"\"\"^^xsd:string ;" %(table["sparql"]) )
        for n in variableList:
            print(" set:queryVariable " + myns + ":" + n + "Variable;")
        print(".\r\n")

        ##create a table according to table design
        print("%s:%sTable\r\n" % (myns, table["name"] ) +
                  "rdf:type set:Table ;\r\n" +
                "set:tableDesign %s:%sTableDesign ;\r\n" %(myns, table["name"]) +
                  "mmm:modeledAs mmm:BusinessEntity ;\r\n"
                  "rdfs:label '%s Table' ; .\r\n" %table["name"] )

    tableSpecInfile.close()
    fout.close()


