import json
import sys
import modules as wsCreate
from CreateForm import  createControlSpec
from jsoncomment import JsonComment


def createFunctionForm(specs):
    parser = JsonComment(json)

    orig_stdout = sys.stdout
    with open('workspace-spec') as infile:
        specs = parser.load(infile)
    fout = open(specs["formOutFile"], 'w')
    sys.stdout = fout

    baseUrl=specs["baseUrl"]
    myns = specs['ns']

    funcFormSpecFile = specs['funcFormSpecFile']
    with open(funcFormSpecFile) as funcFormSpecInfile:
        fcSpecs = parser.load(funcFormSpecInfile, strict=False)

    wsCreate.printHeader(baseUrl, "ffm")

    ## Usage ns, rootClass, type, actions, fieldConrols
    for fm in list(fcSpecs['form']):
        createFuncForm(myns,
                           fm["rootPanelSpec"]["rootClass"],
                           fm["rootPanelSpec"]["action"],
                           fm["type"],
                           fm["rootPanelSpec"]["fieldControl"]  )

    funcFormSpecInfile.close()
    infile.close()
    fout.close()


## create object form
def createFuncForm(myns, ontClassIri, actions, type, fieldControls):
    entityName = ontClassIri.split("#")[-1].replace(">","")
    print("%s:%sFormSpec\r\n" % (myns, entityName) )
    type is "FunctionForm" and print("rdf:type form:FunctionFormSpec ;")
    type is "TemplateForm" and print("rdf:type form:TemplateFormSpec ;")

    createPanel(myns, entityName, ontClassIri, actions, type, fieldControls)

def createPanel(myns, name, ontClassIri, actions, type, fieldControls):
    print("form:rootPanelSpec %s:%sPanelSpec ; \r\n" %(myns,name) +
    "rdfs:label 'Create %s form spec'  .\r\n" %name )

    print("%s:%sPanelSpec\r\n" %(myns,name) +
          "rdf:type form:SimplePanelSpec ;\r\n"+
          "form:rootClass %s;" %(ontClassIri) )

    for k in fieldControls:
        print("form:controlSpec %s:%sFieldSpec ;" %(myns, k["name"] ) )
    print("rdfs:label 'Create %s panel spec' .\r\n" % name)

    for i, k in enumerate(fieldControls):
        print("%s:%sFieldSpec " %(myns,k["name"]))
        createControlSpec(myns, k["name"], i+1, k["dimension"], k["cardinality"], k["range"]  )
        try:
            if k["childPanelSpec"]:
                createPanel(myns, k["childPanelSpec"]["name"], k["childPanelSpec"]["rootClass"], k["childPanelSpec"]["action"],k["type"], k["childPanelSpec"]["fieldControl"])
        except: print("\r\n")

createFunctionForm("")