import sys
import json
from rdflib import URIRef
import modules as wsCreate

def createWs(specs):
    fout = open(specs["workspaceOutFile"], 'w')
    orig_stdout = sys.stdout
    sys.stdout = fout

    baseUrl = specs["baseUrl"]
    role = specs["defaultRole"]
    myns = specs["ns"]
    myvocab = specs["vocab"]
    user = specs["defaultUser"]
    userName = specs["defaultUserName"]
    rootNoun = specs["wsRootNoun"]
    myDatasetTemplate = specs["datasetTemplate"]
    myDatasetDefinition = specs["datasetDefinition"]
    verbList = list(specs['EntityVerb'])

    wsCreate.printHeader(baseUrl, "ws")

    wsCreate.createContextVariableInputParameterBinding(myns, 'wsNoun')
    wsCreate.createContextVariableInputParameterBinding(myns, 'dsNoun')
    wsCreate.createContextVariableInputParameterBinding(myns, 'dpNoun')

    wsCreate.createVocab(myns, myvocab)
    wsCreate.createAndBindUserToRole(myns, user, userName, role)
    feederQuery = wsCreate.createFeederQuery(myns)
    wsCreate.DatasetTemplate(myns, myDatasetTemplate, rootNoun, myvocab, feederQuery, 'wsNoun')
    wsCreate.createDatasetDefinition(myns, myDatasetDefinition, myDatasetTemplate, myvocab, user, feederQuery)

    myQueueWindowTemplateIri=[]   # myObBrowserIri, mySampleWindowTemplate
    myEntityWindowTemplateIri=[] #myObBrowserIri


    wsCreate.createTableListQuery(myns)

    ## create default lists
    defaultListDesign =[]
    defaultList = list(specs['defaultList'])
    for item in defaultList:
        values = item.get('values')
        if values:
                wsCreate.ListDesign(myns, item['name'], values['nounQueries'], values['nounEntities'])
                defaultListDesign.append(myns +":"+item['name'])

    ##create wss
    widgetIris = []
    workspaces = list(specs['WorkspaceTemplate'])
    for item in workspaces:
        wsName = item['name']
        allowsCreationOf = item['allowsCreationOf']
        wsRootNoun = item['wsRootNoun']
        openByVerb = item['openByVerb']
        windowTemplates = list(item['windowTemplate'])

        for widget in windowTemplates:
            widgetName = widget['name']
            widgeNounEntity = widget['nounEntity']
            widgeNounQuery = widget['nounQuery']
            widgetTemplate = wsCreate.WindowTemplate(myns, widgetName, widgeNounEntity, widgeNounQuery)
            widgetTemplate.createWindowDimension(widget['dimension'][0], widget['dimension'][1],widget['dimension'][2],widget['dimension'][3])
            widgetIris.append(URIRef(baseUrl + widgetName))

        if item['type'] == "QueueWorkspaceTemplate":
            wsCreate.QueueWorkspaceTemplate( myns, wsName, defaultListDesign,  role, allowsCreationOf, URIRef(baseUrl + myDatasetTemplate), widgetIris)
        elif item['type'] == "EntityWorkspaceTemplate":
            wsCreate.EntityWorkspaceTemplate( myns, wsName, None,  role, allowsCreationOf, URIRef(baseUrl + myDatasetTemplate),  wsRootNoun, widgetIris)
        elif item['type'] == "DashboardTemplate":
            wsCreate.DashboardTemplate(myns, wsName, None, role, allowsCreationOf, URIRef(baseUrl + myDatasetTemplate), wsRootNoun, widgetIris)
        widgetIris.clear()

        ## create verb binding
    #    if wsRootNoun and (openByVerb !="none" ):
    #        wsCreate.createVerbBinding(myns, wsName, role, wsRootNoun, "OpenWorkSpace", openByVerb, myvocab)

    fout.close()

