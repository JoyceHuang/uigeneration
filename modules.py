import rdflib
import string
from rdflib import URIRef

def printHeader(baseUrl, type):
    baseUrl = baseUrl.replace("#", "")
    print("# baseURI: %s%s\r\n" %(baseUrl, type))
    print(
        "@prefix DummyData: <http://nextangles.com/KYC/DummyData#> . \r\n" + "@prefix owl: <http://www.w3.org/2002/07/owl#> . \r\n" +
        "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . \r\n" +
        "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> . \r\n" + "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> . \r\n" +
        "@prefix set: <http://nextangles.com/Tech/Sets#> . \r\n" + "@prefix ws: <http://nextangles.com/Tech/Workspace#> . \r\n" +
        "@prefix verb: <http://nextangles.com/Tech/verb#> . \r\n" + "@prefix user: <http://nextangles.com/Tech/user#> .\r\n" +
        "@prefix mmm: <http://nextangles.com/Tech/metametamodel#> .\r\n" + "@prefix owllist: <http://nextangles.com/Tech/OWLList#> .\r\n" +
        "@prefix form: <http://nextangles.com/Tech/Form#> .\r\n" +
        "@prefix KYC-verbs: <http://nextangles.com/KYC/UI/KYC-verbs#> .\r\n" +
        "@prefix KYC-tables:<http://nextangles.com/KYC/UI/KYC-tables#> .\r\n")

    print("<http://nextangles.com/KYC/UI/KYC-verbs%s>\r\n" %type +
          "rdf:type owl:Ontology ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/Sets> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/Workspace> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/businessverbs> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/sessionverbs> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/verb> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/function> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/metametamodel> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/Form> ;\r\n" +
          "owl:imports <http://nextangles.com/Tech/user#> ;\r\n" +
          "owl:versionInfo 'Created with TopBraid Composer'^^xsd:string ;\r\n" +
          ".\r\n")

# create vocab
def createVocab(myns, myvocab):
    print("%s:%s\r\n" % (myns, myvocab) + "rdf:type mmm:Vocabulary  .\r\n")

###create verb
def createVerb(myverb, myns):
    print("%s:%s\r\n" % (myns, myverb) +
          "rdf:type verb:EntityVerb ;\r\n" +
          "mmm:modeledAs mmm:BusinessEntity ;\r\n" +
          "rdfs:label '%s'" % (myverb) + "^^xsd:string  .\r\n")

##create verb binding
def createVerbBinding(myns, item):

    if "name" in item:
        verb = item['name']
    if "value" in item:
        ontclass = item['value']
    if "ui" in item:
        ui = item['ui']
    if "role" in item:
        role = item['role']
    if "vocab" in item:
        vocab = item['vocab']
    if "method" in item:
        method = item['method']
    if "changeSetDefinition" in item:
        changeSetDefinition = item['changeSetDefinition']
    if "assertionQuery" in item:
        assertionQuery = item['assertionQuery']

    print("%s:%sVerbBinding\r\n" % (myns, verb) +
        "rdf:type verb:BusinessBinding ; " )
    method == "OpenWorkSpace" and print("set:parameterBinding %s:wsNounContextVariableBinding ; " %myns)
    method == "OpenWorkSpace" and print("ws:boundToWorkspaceTemplate %s:%s ;" %(myns, ui))
    method == "OpenForm" and print("form:boundToFormSpec %s:%s ;" %(myns, ui))
    method == "InvokeDeltaSet" and print("verb:boundToChangesetDefinition %s:%s ;" %(myns, changeSetDefinition)) and createChangeSetDefinition(myns, changeSetDefinition, assertionQuery)

    print("user:boundToRole %s:%s ;" % (myns, role))
    print("verb:boundToClass %s ;" % (ontclass))
    print("verb:boundToMethod <http://nextangles.com/Tech/businessverbs#%s> ; " % (method))
    print( "verb:boundToVerb %s:%s ; \r\n" %(myns, verb) +
          "verb:boundToVocabulary %s:%s ; \r\n" %(myns, vocab) +
          "rdfs:label '%s binding'^^xsd:string  .\r\n"%verb )


def createChangeSetDefinition(myns, changesetDefinition, assertionQuery):
    print( "%s:%s\r\n" %(myns, changesetDefinition) +
    "rdf:type set:ChangeSetDefinition; \r\n" +
    "set:querySet %s:%sQuerySet ;\r\n" %(myns, changesetDefinition) +
    "rdfs:label '%s' .\r\n" % changesetDefinition )
    assertionQuery != "none" and  createQuerySet(myns, changesetDefinition, "assertion", assertionQuery)

def createQuerySet(ns, name, type, sparqls):
    print("\r\n%s:%sQuerySet \r\n"%(ns, name) +
    "rdf:type set:OrderedQuerySet ;\r\n" +
    "owllist:First %s:%s%sQuery ;\r\n" %(ns, name, type) +
    "owllist:Next owllist:nil  .\r\n")
    sparqls != "none" and createQuery(ns, name, type, sparqls)

def createQuery(ns, name, type, sparql):
    print("\r\n%s:%s%sQuery" %(ns, name, type) )
    type is "assertion" and print("rdf:type set:AssertionQuery ;")
    type is "select" and print("rdf:type set:SelectQuery ;")
    print("set:SPARQLNotation \"\"\"%s\"\"\"^^xsd:string ;"%sparql)
    print("rdfs:label '%s %s query' .\r\n" % (name, type))


##create role, user and bound user to a role
def createAndBindUserToRole(myns, user, userName, role):
    print("<http://nextangles.com/Tech/user#Role>\r\nmmm:modeledAs mmm:BusinessClass .\r\n")
    print("<http://nextangles.com/Tech/user#User>\r\nmmm:modeledAs mmm:BusinessClass .\r\n")
    print("<http://nextangles.com/Tech/user#boundToRole>\r\nmmm:modeledAs mmm:Relationship .\r\n")
    # define role
    print("%s:%s" % (myns, role) + "\r\n" + "rdf:type user:Role ;\r\n" + "mmm:modeledAs mmm:BusinessEntity ;")
    print("rdfs:label '%s' . " % role)
    # define user
    print("\r\n%s:%s" % (myns, user))
    print("rdf:type user:User ;\r\nmmm:modeledAs mmm:BusinessEntity ;")
    print("user:boundToRole %s:%s ;" % (myns, role))
    print("user:username '%s'^^xsd:string ;" % userName)
    print("rdfs:label '%s' .\r\n" % userName)


##create object browser window template
class WindowTemplate:
    def __init__(self, ns, name, nounEntity, nounQuery):
        print(
            "%s:%s\r\n" % (ns, name) +
            "rdf:type ws:WindowTemplate ;\r\n" +
            "ws:associatedVerb <http://nextangles.com/Tech/sessionverbs#View> ;\r\n" +
            "rdfs:label '%s' ;" % name)
        if nounEntity is None:
            print("ws:nounQuery %s ;" % (nounQuery))
        else:
            print("ws:nounEntity %s ;" % (nounEntity))

    def createWindowDimension(self, xpos, ypos, colspan, height):
        print("ws:xPosition %s ;\r\n" % (xpos) +
              "ws:yPosition %s ;\r\n" % (ypos) +
              "ws:columnSpan %s ;\r\n" % (colspan) +
              "ws:height %s . \r\n" % (height))


##create defaultList
class ListDesign:
    def __init__(self, ns, name, assoQueries, listItem):
        print("%s:%s \r\n " % (ns,name) +
              " rdf:type set:ListDesign ;")
        if assoQueries != None:
            for i in assoQueries:
                print(" set:associatedQuery %s ; " % i)
        if listItem != None:
            for i in listItem:
                print(" set:listItem %s ;" % i)
        print(" rdfs:label '%s'  . \r\n" % name)


def createTableListQuery(myns):
    print("%s:TableListQuery \r\n" % myns +
          "rdf:type set:SelectQuery ; \r\n" +
          "set:SPARQLNotation \"\"\"PREFIX set:<http://nextangles.com/Tech/Sets#> " +
          "SELECT  DISTINCT ?table WHERE { ?table a set:Table . }\"\"\"^^xsd:string ; \r\n" +
          "rdfs:label 'table list query' .\r\n")


## ?wsNoun,?dsNoun,?dpNoun variable
def createContextVariableInputParameterBinding(myns, contextNoun):
    print("%s:%sContextVariableBinding\r\n" % (myns, contextNoun) +
          "rdf:type set:InputParameterBinding ;\r\n" +
          # "set:boundToIRI %s:dpNoun ; \r\n"% myns +
          "set:boundToContextVariable '?%s' .\r\n" % (contextNoun))


##create DatasetTemplate
class DatasetTemplate:
    def __init__(self, myns, name, associatedNoun, vocab, feederQuery, contextNoun):
        print("%s:%s \r\n " % (myns, name) +
              " rdf:type set:DatasetTemplate ;\r\n" +
              " set:associatedNoun %s ; \r\n" % associatedNoun +
              " set:associatedVocabulary %s:%s ; " % (myns, vocab) )
        print(" set:parameterBinding %s:%sContextVariableBinding ; " % (myns, contextNoun))
        print(" set:querySet %s:%s ;" % (myns, feederQuery) +
              " rdfs:label 'dataset template' .\r\n")


## create feederQuerySet
def createFeederQuery(myns):
    print("%s:AllResourceQuerySet\r\n" % myns +
          "rdf:type set:OrderedQuerySet ;\r\n" +
          "owllist:First %s:AllResourcesSelectQuery;\r\n" % myns +
          "owllist:Next owllist:nil  .\r\n")
    print("%s:AllResourcesSelectQuery\r\n" % myns +
          "rdf:type set:SelectQuery ; \r\n" +
          "set:SPARQLNotation \"\"\"CONSTRUCT { ?s ?p ?o. ?o ?p2 ?o2 . ?o2 ?p3 ?o3 . } WHERE{ { ?s ?p ?o .} {?o ?p2 ?o2 . } { ?o2 ?p3 ?o3 . } \r\n"+
          " FILTER (?s = <http://nextangles.com/KYC/DummyData#StarTrustBank> ) \r\n FILTER (?p != <http://nextangles.com/Tech/metametamodel#modeledAs> && \r\n"+
            "?p2 not in (<http://nextangles.com/Tech/metametamodel#modeledAs>, <http://www.w3.org/2000/01/rdf-schema#subClassOf>) && \r\n"+
        "?p3 not in (<http://nextangles.com/Tech/metametamodel#modeledAs>, <http://www.w3.org/2000/01/rdf-schema#subClassOf> )) }\"\"\"^^xsd:string. \r\n")
    return "AllResourceQuerySet"


# create dataset definition
def createDatasetDefinition(myns, myDatasetDefinition, myDatasetTemplate, myvocab, user, feederQuery):
    print("%s:%s\r\n" % (myns, myDatasetDefinition) +
          "rdf:type set:DatasetDefinition ;\r\n" +
          "set:associatedVocabulary %s:%s ;\r\n" % (myns, myvocab) +
          "set:forTemplate %s:%s ;\r\n" % (myns, myDatasetTemplate) +
          "set:forUser %s:%s;\r\n" % (myns, user) +
          "set:name 'get all resources'^^xsd:string ;\r\n" +
          "set:querySet %s:%s ;\r\n" % (myns, feederQuery) +
          "rdfs:label 'dataset definition' ; .\r\n")


## create ws
class WsTemplate(object):
    def __init__(self, wsType, myns, wsName, defaultListIri, roleClass, allowsCreationOf, datasetTemplateIri,
                 rootNounClass, windowTemplateIri):
        if isinstance(wsName, URIRef):
            print("<%s>" % str(wsName))
        else:
            print("%s:%s" % (myns, wsName))
        print(" rdf:type ws:%s ;" % (wsType))
        if (defaultListIri is not None):
            for item in defaultListIri:
                if item is not None:
                    print("  ws:defaultList %s ;" % item)
        print("  user:availableForRole %s:%s ;\r\n" % (myns, roleClass) +
              "  ws:dataSetTemplate <%s> ;\r\n" % datasetTemplateIri +
              "  ws:rootNounClass %s ;" % rootNounClass)
        if (allowsCreationOf is not None):
            for item in allowsCreationOf:
                print("  ws:allowsCreationOf %s; " % item)
        if (windowTemplateIri is not None):
            for item in windowTemplateIri:
                if isinstance(item, URIRef):
                    print("  ws:windowTemplate <%s>; " % item)
                else:
                    print("  ws:windowTemplate %s; " % item)
            print(" rdfs:label '%s'  . \r\n" %wsName)


# create ws subclass
class DashboardTemplate(WsTemplate):
    def __init__(self, ns, wsName, defaultList, roleClass, allowsCreationOf, datasetTemplateIri, windowTemplateIri):
        WsTemplate.__init__(self, "DashboardTemplate", ns, wsName, defaultList, roleClass, allowsCreationOf,
                            datasetTemplateIri, "user:User", windowTemplateIri)

# create ws subclass
class QueueWorkspaceTemplate(WsTemplate):
    def __init__(self, ns, wsName, defaultList, roleClass, allowsCreationOf, datasetTemplateIri, windowTemplateIri):
        WsTemplate.__init__(self, "QueueWorkspaceTemplate", ns, wsName, defaultList, roleClass, allowsCreationOf,
                            datasetTemplateIri, "user:User", windowTemplateIri)

# create ws subclass
class EntityWorkspaceTemplate(WsTemplate):
    def __init__(self, ns, wsName, defaultList, roleClass, allowsCreationOf, datasetTemplateIri, rootNounClass,
                 windowTemplateIri):
        WsTemplate.__init__(self, "EntityWorkspaceTemplate", ns, wsName, defaultList, roleClass, allowsCreationOf,
                            datasetTemplateIri, rootNounClass, windowTemplateIri)
