from CreateTable import createTable
from CreateWs import  createWs
from CreateForm import  createForm
import sys
import json
from rdflib import URIRef
import modules as wsCreate
from jsoncomment import JsonComment

def createVerb(specs):

    fout = open(specs["verbOutFile"], 'w')
    orig_stdout = sys.stdout
    sys.stdout = fout

    baseUrl = specs["baseUrl"]
    role = specs["defaultRole"]
    myns = specs["ns"]
    myvocab = specs["vocab"]
    user = specs["defaultUser"]
    userName = specs["defaultUserName"]

    myDatasetTemplate = specs["datasetTemplate"]
    myDatasetDefinition = specs["datasetDefinition"]
    rootNoun = specs["wsRootNoun"]
    verbList = list(specs['EntityVerb'])

    wsCreate.printHeader(baseUrl, "verbs")

    for item in verbList:
        wsCreate.createVerb(item['name'], myns)
        wsCreate.createVerbBinding(myns, item)


    fout.close()
